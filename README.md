# GitHub Explorer-example

- This is an example project using Kotlin, MVP, RxJava2, Dagger2, Retrofit2.
- It uses GitHubAPI and contains: authentication, searching repositories, repository and owner details, current logged in user details.

**In order to run this app you need to get your Client ID and Client Secret key on https://github.com**

## Api Docs

- https://developer.github.com/v3/
- https://api.github.com/
- https://developer.github.com/v3/search/#search-repositories
- https://developer.github.com/apps/building-oauth-apps/authorizing-oauth-apps/

## Screenshots
Search: http://i66.tinypic.com/24aybl3.jpg
Repository details: http://i65.tinypic.com/2wee5nr.jpg
Owner details: http://i65.tinypic.com/ehgmqx.jpg
User details: http://i63.tinypic.com/hsqg7q.jpg


